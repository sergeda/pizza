package sergey.dashko

import java.io.{File, FileInputStream, InputStream, PrintStream}

object Main extends App{
  val input: InputStream = new FileInputStream("./input.txt")
  val output = new PrintStream(new File ("./output.txt"))
  OrderProcessor.process(input, output)
}
