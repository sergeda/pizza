package sergey.dashko

import java.io.{BufferedReader, InputStream, InputStreamReader, PrintStream}

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

case object LineEndException extends Exception

object OrderProcessor {
  type StartTime = Int
  type CookTime = Int
  case class Order (startTime: StartTime, cookTime: CookTime)

  def process(in: InputStream, out: PrintStream): Unit = {
    val input = new BufferedReader(new InputStreamReader(in))

    def getOrder(line: String): Try[Order] = {
      val data = line.split(" ")
      Try(Order(data.head.toInt, data.head.head.toInt))
    }

    def getOrder: Try[Order] = for {
      ln <- getLine()
      ord <- getOrder(ln)
    } yield ord

    def getLine(): Try[String] = {
      val line = Try(input.readLine())
      line.flatMap{ ln =>
        if(ln == null) Failure(LineEndException) else Success(ln)
      }
    }

    def getOrdersUntilTime(time: StartTime): Vector[Order] = {
      @tailrec
      def rec(currentOrder: Try[Order], accum: Vector[Order]): Vector[Order] = {
        currentOrder match {
          case Success(order: Order) =>
            if(order.startTime <= time) {
              rec(getOrder, accum :+ order)
            } else {
              accum :+ order
            }
          case Failure(_) => accum
        }
      }
      rec(getOrder, Vector())
    }

    @tailrec
    def bake(startTime: StartTime, currentOrder: Order, ordersQueue: Vector[Order], waitingTimeAccum: Int): Int = {
      if (ordersQueue.isEmpty){
        waitingTimeAccum
      } else {
        val bakeCanBeStarted = if (currentOrder.startTime > startTime)
          currentOrder.startTime
        else startTime

        val waitingTime = if (currentOrder.startTime < startTime)
          currentOrder.cookTime + startTime - currentOrder.startTime
        else currentOrder.cookTime

        val nextStartTime = bakeCanBeStarted + currentOrder.cookTime

        val sortedQueue = (getOrdersUntilTime(nextStartTime) ++ ordersQueue).sortBy(_.cookTime)
        bake(nextStartTime, sortedQueue.head, sortedQueue.tail, waitingTimeAccum + waitingTime)
      }
    }




    val clientCount: Try[Int] = Try(input.readLine().toInt)

    val result = clientCount.flatMap { clCount =>
      val totalWait = for {
        ln <- getLine()
        order <- getOrder(ln)
      } yield bake(order.startTime, order, getOrdersUntilTime(order.startTime + order.cookTime), 0)
      totalWait.map{_ / clCount}
    }



  }


}
